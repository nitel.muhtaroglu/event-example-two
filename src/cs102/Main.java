package cs102;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 200);
        frame.setLayout(new FlowLayout());

        JButton button1 = new JButton("OK");
        JButton button2 = new JButton("Cancel");

        AnotherActionListener listener1 = new AnotherActionListener(1);
        AnotherActionListener listener2 = new AnotherActionListener(2);
        AnotherActionListener listener3 = new AnotherActionListener(3);

        button1.addActionListener(listener2);
        button1.addActionListener(listener1);
        button2.addActionListener(listener2);
        button2.addActionListener(listener3);

        frame.add(button1);
        frame.add(button2);
        frame.setVisible(true);
    }
}
