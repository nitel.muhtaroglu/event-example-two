package cs102;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AnotherActionListener implements ActionListener {
    private int id;

    public AnotherActionListener(int id) {
        this.id = id;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() instanceof JButton) {
            JButton button = (JButton) actionEvent.getSource();
            System.out.println("Listener: " + this.id + " from " + button.getText());
            /* Alternative: System.out.println("Listener: " + this.id + " from " + ((JButton) actionEvent.getSource()).getText()); */
        }
    }
}
